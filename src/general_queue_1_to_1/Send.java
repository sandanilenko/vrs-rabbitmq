package general_queue_1_to_1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Created by sandan on 28.05.16.
 */
public class Send {
    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv)
        throws java.io.IOException {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("danilenkoA");
            factory.setPassword("gBn65#2");
            factory.setVirtualHost("danilenkoA-host");
            factory.setHost("84.237.4.179");
            factory.setPort(5672);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "Hello World!";
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");

            channel.close();
            connection.close();
        }
}
